###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import combinations_with_replacement as comb
from itertools import product

import pytest

from hadron_names import Hadron, name_hadron
from hadron_names.exceptions import (
    GParityError,
    IsospinError,
    ParityError,
    QuarkContentError,
    SpinError,
)
from hadron_names.utils import get_possible_isospins, is_quark, names_to_particles

QUARKS = "duscb"


@pytest.mark.parametrize("quark,parity,g_parity", product(QUARKS, *[[+1, -1]] * 2))
def test_flavourless_isospin_zero_meson(quark, parity, g_parity):
    quarks = names_to_particles([quark, f"anti-{quark}"])
    hadron = Hadron(quarks, 0, parity, g_parity, two_isospin=0)
    print(name_hadron(hadron))


@pytest.mark.parametrize(
    "q1,q2,parity,g_parity", product(*[list(QUARKS[:2])] * 2, *[[+1, -1]] * 2)
)
def test_flavourless_isospin_one_meson(q1, q2, parity, g_parity):
    quarks = names_to_particles([q1, f"anti-{q2}"])
    hadron = Hadron(quarks, 0, parity, g_parity, two_isospin=2)
    print(name_hadron(hadron))


@pytest.mark.parametrize(
    "q1,q2",
    filter(
        lambda quarks: quarks[0] != quarks[1] and not all([q in "ud" for q in quarks]),
        product(QUARKS, repeat=2),
    ),
)
def test_flavoured_meson(q1, q2):
    quarks = names_to_particles([q1, f"anti-{q2}"])
    hadron = Hadron(quarks, 0, -1)
    print(name_hadron(hadron))


@pytest.mark.parametrize("q1,q2,q3", comb(QUARKS, 3))
def test_baryon(q1, q2, q3):
    quarks = names_to_particles([q1, q2, q3])
    antiquarks = [q.invert() for q in quarks]
    for two_isospin in get_possible_isospins(quarks):
        for quark_content in quarks, antiquarks:
            hadron = Hadron(quark_content, 1, -1, two_isospin=two_isospin)
            print(name_hadron(hadron))


def xfail_if_isospin_above(limit: int, two_isospin: int):
    if (I := two_isospin / 2) > limit / 2:
        msg = f"Skipping because {I=} " "is not covered by the naming scheme"
        pytest.xfail(msg)


@pytest.mark.parametrize(
    "q1,q2,q3,q4", map(lambda x: (*x[0], *x[1]), comb(product(QUARKS, repeat=2), 2))
)
def test_tetraquark(q1, q2, q3, q4):
    q2bar = f"anti-{q2}"
    q4bar = f"anti-{q4}"

    quarks = names_to_particles([q1, q2bar, q3, q4bar])
    possible_isospins = get_possible_isospins(quarks)
    xfail_if_isospin_above(2, min(possible_isospins))
    allowed_isospins = filter(lambda i: i <= 2, possible_isospins)
    for two_isospin in allowed_isospins:
        hadron = Hadron(quarks, 0, -1, -1, two_isospin=two_isospin)
        print(name_hadron(hadron))


@pytest.mark.parametrize(
    "q1,q2,q3,q4,q5", map(lambda x: (*x[0], x[1]), product(comb(QUARKS, 4), QUARKS))
)
def test_pentaquark(q1, q2, q3, q4, q5):
    qbar = f"anti-{q5}"

    quarks = names_to_particles([q1, q2, q3, q4, qbar])
    antiquarks = [q.invert() for q in quarks]
    possible_isospins = get_possible_isospins(quarks)
    xfail_if_isospin_above(3, min(possible_isospins))
    allowed_isospins = filter(lambda i: i <= 3, possible_isospins)
    for two_isospin in allowed_isospins:
        for quark_content in quarks, antiquarks:
            hadron = Hadron(quark_content, 1, -1, two_isospin=two_isospin)
            print(name_hadron(hadron))


@pytest.mark.parametrize(
    "quarks,two_spin,parity,g_parity,two_isospin,expected",
    [
        ("uud", 1, +1, -1, 1, "p"),
        ("udd", 1, +1, -1, 1, "n"),
        (["c", "anti-c"], 2, -1, -1, 0, r"J/\psi"),
    ],
)
def test_specific_name(quarks, two_spin, parity, g_parity, two_isospin, expected):
    quarks = names_to_particles(quarks)
    hadron = Hadron(quarks, two_spin, parity, g_parity, two_isospin=two_isospin)
    name = name_hadron(hadron)
    if isinstance(name, list):
        assert expected in name
    elif isinstance(name, str):
        assert expected == name
    else:
        raise RuntimeError(f"{name=} is neither a list nor a string")


@pytest.mark.parametrize("quark_name", list(QUARKS) + [f"anti-{q}" for q in QUARKS])
def test_is_quark(quark_name, expected=True):
    assert is_quark(quark_name) == expected


@pytest.mark.parametrize(
    "quarks,two_spin,parity,g_parity,two_isospin,expected",
    [
        ("uu", 0, -1, -1, 0, QuarkContentError),
        (["u", "anti-d"], 0, -1, -1, 1, IsospinError),
        (["u", "anti-u"], 1, -1, -1, 0, SpinError),
        (["u", "anti-u"], 0, 0, -1, 0, ParityError),
        (["u", "anti-u"], 0, -1, 0, 0, GParityError),
        (["u", "anti-u"], 0, -1, None, 0, GParityError),
    ],
)
def test_exceptions(quarks, two_spin, parity, g_parity, two_isospin, expected):
    quarks = names_to_particles(quarks)
    try:
        Hadron(quarks, two_spin, parity, g_parity, two_isospin=two_isospin)
    except expected:
        return
    raise RuntimeError(f"Expected exception {expected} not encountered")
