# Development guide

## Local web development

### Building for the web

A `.whl` file must be build and placed in the `public/` folder:

```
python setup.py bdist_wheel -d "public"
```

#### Building pyscript

If using unreleased features of PyScript, it needs to be built from scratch:

```
git clone https://github.com/pyscript/pyscript.git
cd pyscript/pyscriptjs
npm install
npm run build
mv build/* ../../public/
```

### Serving the page

The page must be served over HTTP to avoid [a CORS error](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp).
A simple way to achieve this is using the python `http` module:

```
cd public/
python -m http.server
```

then navigate to <http://0.0.0.0:8000/>.

If you use NoScript, uMatrix or similar, third-party JS must be allowed from:

- `cdn.jsdelivr.net`
- `pyscript.net`

and XHR from:

- `cdn.jsdelivr.net`
- `pyscript.net`
- `pypi.org`
- `files.pythonhosted.org`
