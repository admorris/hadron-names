###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from .conventional import get_light_flavourless_meson_symbol
from .exceptions import IsospinError
from .hadron import Hadron
from .utils import get_charge_symbol

FLAVOURED_TETRAQUARK_SUPERSCRIPT = {
    # Index: (parity, two_isospin)
    (-1, 0): r"\eta",
    (-1, 1): r"\tau",
    (-1, 2): r"\pi",
    (+1, 0): "f",
    (+1, 1): r"\theta",
    (+1, 2): "a",
}

PENTAQUARK_SUPERSCRIPT = {
    # Index: two_isospin
    0: r"\Lambda",
    1: "N",
    2: r"\Sigma",
    3: r"\Delta",
}


def raise_if_isospin_above(limit: float, hadron: Hadron):
    if (I := hadron.isospin) > limit:
        raise IsospinError(
            f"Scheme does not cover {I=}"
            f" ({''.join([str(q) for q in hadron.quarks])})"
        )


def get_subscript(hadron: Hadron) -> str:
    subscript = ""

    if hadron.hidden_beauty:
        subscript += r"\Upsilon" * min(
            hadron.count_quarks("b"), hadron.count_antiquarks("b")
        )
    if hadron.hidden_charm:
        subscript += r"\psi" * min(
            hadron.count_quarks("c"), hadron.count_antiquarks("c")
        )
    if hadron.hidden_strangeness:
        subscript += r"\phi" * min(
            hadron.count_quarks("s"), hadron.count_antiquarks("s")
        )
    subscript += "{}"  # Terminate last LaTeX command for Greek letters

    if hadron.open_beauty:
        subscript += "b" * abs(hadron.beauty)
    if hadron.open_charm:
        symbol = "c"
        if hadron.beauty * hadron.charm > 0:
            symbol = r"\overline{c}"
        subscript += symbol * abs(hadron.charm)
    if hadron.open_strangeness:
        symbol = "s"
        if (
            hadron.beauty * hadron.strangeness < 0
            or hadron.charm * hadron.strangeness > 0
        ):
            symbol = r"\overline{s}"
        subscript += symbol * abs(hadron.strangeness)

    # {} not needed at start or end of string
    return subscript.removeprefix("{}").removesuffix("{}")


def name_tetraquark(hadron: Hadron) -> tuple[str, str, str, bool]:
    raise_if_isospin_above(1, hadron)

    primary_symbol = "T"

    superscript = ""
    if hadron.is_flavourless:
        equivalent_flavourless_meson = get_light_flavourless_meson_symbol(hadron)
        superscript += equivalent_flavourless_meson.split(",")[-1]
    else:
        PI = (hadron.parity, hadron.two_isospin)
        superscript += FLAVOURED_TETRAQUARK_SUPERSCRIPT[PI]
    superscript += get_charge_symbol(hadron.charge)

    subscript = get_subscript(hadron)
    subscript += str(int(hadron.spin))

    return primary_symbol, superscript, subscript


def name_pentaquark(hadron: Hadron) -> tuple[str, str, str, bool]:
    raise_if_isospin_above(1.5, hadron)

    primary_symbol = "P"

    superscript = ""
    superscript += PENTAQUARK_SUPERSCRIPT[hadron.two_isospin]
    superscript += get_charge_symbol(hadron.charge)

    subscript = get_subscript(hadron)

    return primary_symbol, superscript, subscript
