###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Literal, Optional
from warnings import warn

from particle import Particle
from particle.pdgid.functions import is_quark

from .exceptions import (
    GParityError,
    IsospinError,
    ParityError,
    QuarkContentError,
    SpinError,
)
from .utils import get_charge_symbol, get_possible_isospins


class Hadron:
    def __init__(
        self,
        quarks: list[Particle],
        two_spin: int,
        parity: Literal[+1, -1],
        g_parity: Optional[Literal[+1, -1]] = None,
        two_isospin: Optional[int] = None,
    ):
        assert all(map(lambda q: is_quark(q.pdgid), quarks))
        self.quarks = quarks
        self.two_spin = two_spin
        if not self.is_confined:
            raise QuarkContentError("This combination of quarks is not confined")
        if len(self.quarks) % 2 != self.two_spin % 2 or self.two_spin < 0:
            raise SpinError(
                f"J={two_spin/2:.1f} not allowed for a {len(quarks)}-quark combination"
            )
        # Sanity check before setting P and C
        if parity not in [-1, +1]:
            raise ParityError(f"Parity must be ±1, not {parity}")
        if g_parity is not None:
            if g_parity not in [-1, +1]:
                raise GParityError(f"C-parity must be ±1, not {g_parity}")
        elif self.is_meson and self.is_flavourless:
            raise GParityError("G-parity must be specified for a flavourless meson")
        self.parity = parity
        self.g_parity = g_parity
        # Deduce isospin if possible and not already given
        if two_isospin is None:
            self.two_isospin = self.possible_isospins[0]
            if len(self.possible_isospins) > 1:
                warn(
                    RuntimeWarning(
                        "Ambiguity encountered when deducing isospin: "
                        f"I in {[i/2 for i in self.possible_isospins]}. "
                        "Please provide this as an argument. "
                        "Using minimum value."
                    )
                )
        else:
            if two_isospin not in self.possible_isospins:
                raise IsospinError(
                    f"I={two_isospin/2:.1f} not allowed for this quark combination"
                )
            self.two_isospin = two_isospin

    @property
    def charge(self) -> int:
        "Total charge of a set of quarks"
        three_charge = sum([q.three_charge for q in self.quarks])
        assert three_charge % 3 == 0
        return int(three_charge / 3)

    @property
    def possible_isospins(self) -> list[int]:
        return get_possible_isospins(self.quarks)

    @property
    def isospin(self) -> float:
        return self.two_isospin / 2.0

    @property
    def c_parity(self) -> Optional[int]:
        if not all([self.is_meson, self.is_flavourless, self.charge == 0]):
            return None
        c_parity = self.g_parity * (-1) ** self.isospin
        return int(c_parity)

    @property
    def spin(self) -> float:
        return self.two_spin / 2.0

    def _flavour(self, flavour: Literal["s", "c", "b"]) -> int:
        "Deduce the flavour quantum numbers S, C or B"
        return sum(
            [
                int(q.three_charge / abs(q.three_charge))
                if q.quarks.lower() == flavour.lower()
                else 0
                for q in self.quarks
            ]
        )

    @property
    def strangeness(self) -> int:
        return self._flavour("s")

    @property
    def charm(self) -> int:
        return self._flavour("c")

    @property
    def beauty(self) -> int:
        return self._flavour("b")

    @property
    def heaviest_flavour(self) -> Optional[int]:
        for flavour in [self._flavour(f) for f in "BCS"]:
            if flavour != 0:
                return flavour
        return None

    def count_quarks(
        self, flavour: Optional[Literal["d", "u", "s", "c", "b"]] = None
    ) -> int:
        return len(
            [
                q
                for q in self.quarks
                if q.three_charge in [2, -1]
                and (flavour is None or q.quarks == flavour.lower())
            ]
        )

    def count_antiquarks(
        self, flavour: Optional[Literal["d", "u", "s", "c", "b"]] = None
    ) -> int:
        return len(
            [
                q
                for q in self.quarks
                if q.three_charge in [-2, 1]
                and (flavour is None or q.quarks == flavour.upper())
            ]
        )

    @property
    def num_quarks(self) -> int:
        return self.count_quarks()

    @property
    def num_antiquarks(self) -> int:
        return self.count_antiquarks()

    @property
    def is_meson(self) -> bool:
        return len(self.quarks) % 2 == 0

    @property
    def is_baryon(self) -> bool:
        return len(self.quarks) % 2 == 1

    @property
    def is_conventional(self) -> bool:
        # Straightforward test for non qq̅ or non qqq
        if len(self.quarks) not in [2, 3]:
            return False
        if self.is_meson and self.is_flavourless and self.charge == 0:
            return self.has_allowed_qq_qns
        return True

    @property
    def has_allowed_qq_qns(self) -> bool:
        "Check J^PC is allowed under qq̅ model"
        # TODO: must q and q̅ be the same flavour?
        if not (self.is_flavourless and len(self.quarks) in [0, 2]):
            return True
        # Abuse the definition of C-parity, which is not defined for Q!=0
        assert self.two_isospin % 2 == 0  # Ensure we don't get complex values
        c_parity = int(self.g_parity * (-1) ** self.isospin)
        if self.parity == c_parity:  # S = 1, so |S-L| ≤ J ≤ S+L
            if self.parity > 0:
                allowed = True  # J^++ is always allowed
            else:
                allowed = self.spin > 0  # J=0 impossible if S=1 and L is even
        else:  # S = 0, so J = L
            if self.parity > 0:  # L is odd, J should be odd
                allowed = self.spin % 2 == 1
            else:  # L is even, J should be even
                allowed = self.spin % 2 == 0
        if not allowed:
            J = str(int(self.spin))
            PC = "".join([get_charge_symbol(x) for x in [self.parity, c_parity]])
            msg = f"Manifestly exotic quantum numbers under qq̅ model: J^PC = {J}^{PC}"
            warn(RuntimeWarning(msg))
        return allowed

    @property
    def is_exotic(self) -> bool:
        return not self.is_conventional

    @property
    def is_confined(self) -> bool:
        return (self.num_quarks - self.num_antiquarks) % 3 == 0

    @property
    def is_flavourless(self) -> bool:
        return all(self._flavour(f) == 0 for f in "scb")

    @property
    def baryon_number(self) -> int:
        return (self.num_quarks - self.num_antiquarks) / 3

    def _hidden_flavour(self, flavour=Literal["s", "c", "b"]) -> bool:
        quark_content_str = "".join([q.quarks for q in self.quarks])
        num_quarks = quark_content_str.count(flavour.lower())
        num_antiquarks = quark_content_str.count(flavour.upper())
        return num_quarks > 0 and num_antiquarks > 0

    @property
    def hidden_strangeness(self) -> bool:
        return self._hidden_flavour("s")

    @property
    def hidden_charm(self) -> bool:
        return self._hidden_flavour("c")

    @property
    def hidden_beauty(self) -> bool:
        return self._hidden_flavour("b")

    @property
    def open_strangeness(self) -> bool:
        return self.strangeness != 0

    @property
    def open_charm(self) -> bool:
        return self.charm != 0

    @property
    def open_beauty(self) -> bool:
        return self.beauty != 0
