###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="action", required=True)
    build_parser = subparsers.add_parser(
        "build",
        description="Print the correct symbol for a hadron, based on its properties",
    )
    #
    build_parser.set_defaults(func=lambda args: build())

    resolve_parser = subparsers.add_parser(
        "resolve", description="Print the properties of a hadron, given its name"
    )
    resolve_parser.set_defaults(func=lambda args: resolve())

    args = parser.parse_args()
    args.func(args)


def build():
    raise NotImplementedError()


def resolve():
    raise NotImplementedError()
