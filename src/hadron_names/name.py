###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Union

from .conventional import alternative_name, name_baryon, name_meson
from .exotic import name_pentaquark, name_tetraquark
from .hadron import Hadron
from .utils import form_name


def name_hadron(hadron: Hadron) -> Union[str, list[str]]:
    # Determine which set of rules to use (i.e. choose a function)
    try:
        n_q = len(hadron.quarks)
        naming_function = {
            0: name_meson,  # Technically glueballs are also covered here
            2: name_meson,
            3: name_baryon,
            4: name_tetraquark,
            5: name_pentaquark,
        }[n_q]
    except KeyError as e:
        msg = f"Scheme does not cover {n_q}-quark hadrons"
        raise NotImplementedError(msg) from e

    # Determine whether the symbol should have an overline
    bar = False
    if hadron.is_meson and hadron.charge == 0 and not hadron.is_flavourless:
        # Flavoured neutral mesons whose heaviest flavour QN is negative
        bar = hadron.heaviest_flavour < 0
    elif hadron.is_baryon:
        # Negative baryon number
        bar = hadron.baryon_number < 0

    # Call the chosen naming function and convert tuple(s) to string(s)
    primary_symbol, superscript, subscript = naming_function(hadron)
    names = [
        form_name(primary, superscript, subscript, bar)
        for primary in primary_symbol.split(",")
    ]
    # Add any traditional/alternative name
    alt_name = alternative_name(hadron)
    if alt_name:
        names += [form_name(*alt_name)]

    # If only a single possible name, return a string, otherwise a list of strings
    if len(names) == 1:
        return names[0]
    return names
