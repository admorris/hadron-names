###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from particle import Particle
from particle.exceptions import MatchingIDNotFound
from particle.pdgid.functions import is_quark as _is_quark


def names_to_particles(names: list[str]) -> list[Particle]:
    "Convert a list of EvtGen names to Particle objects"
    return [Particle.from_evtgen_name(p) for p in names]


def get_possible_isospins(quarks):
    "List of all possible values of isospin that a given set of quarks can have. NB: returns 2*I"
    max_isospin = sum([int(2 * q.I) for q in quarks])
    min_isospin = abs(sum([int(2 * get_Iz(q)) for q in quarks]))
    return list(range(min_isospin, max_isospin + 1, 2))


def is_quark(name: str) -> bool:
    try:
        p = Particle.from_evtgen_name(name)
    except MatchingIDNotFound:
        return False
    return _is_quark(p.pdgid)


def get_charge_symbol(charge: int) -> str:
    "Either 0, + or - (repeated if |charge| > 1)"
    if charge > 0:
        return "+" * charge
    if charge < 0:
        return "-" * -charge
    return "0"


def get_Iz(quark: Particle) -> int:
    "Get the third component of isospin of a quark"
    assert len(quark.quarks) == 1  # make sure this Particle object is a quark
    sign = quark.three_charge / abs(quark.three_charge)  # Sign of charge == sign of Iz
    return sign * quark.I


def form_name(
    primary_symbol: str, superscript: str = "", subscript: str = "", bar: bool = False
) -> str:
    name = primary_symbol

    if bar:
        name = f"\\overline{{{name}}}"
    if len(subscript) > 0:
        name += f"_{{{subscript}}}"
    if len(superscript) > 0:
        name += f"^{{{superscript}}}"
    return name
