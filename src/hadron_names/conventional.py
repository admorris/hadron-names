###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from typing import Optional

from .exceptions import IsospinError
from .hadron import Hadron
from .utils import get_charge_symbol

BOTTOMONIA = {
    # Index: (parity, g_parity)
    (+1, -1): "h",
    (-1, +1): r"\eta",
    (+1, +1): r"\chi",
    (-1, -1): r"\Upsilon",
}

CHARMONIA = {
    # Index: (parity, g_parity)
    (+1, -1): "h",
    (-1, +1): r"\eta",
    (+1, +1): r"\chi",
    (-1, -1): r"\psi",
}

LIGHT_ISOSPIN_ZERO = {
    # Index: (parity, g_parity)
    (+1, -1): "h",
    (-1, +1): r"\eta",
    (+1, +1): "f",
    (-1, -1): r"\omega,\phi",
}

LIGHT_ISOSPIN_ONE = {
    # Index: (parity, g_parity)
    (+1, +1): "b",
    (-1, -1): r"\pi",
    (+1, -1): "a",
    (-1, +1): r"\rho",
}


def get_light_flavourless_meson_symbol(hadron: Hadron) -> str:
    PG = (hadron.parity, hadron.g_parity)
    if hadron.isospin == 0:
        return LIGHT_ISOSPIN_ZERO[PG]
    if hadron.isospin == 1:
        return LIGHT_ISOSPIN_ONE[PG]
    raise IsospinError(f"{hadron.isospin=}")


def name_flavourless_meson(hadron: Hadron) -> tuple[str, str, str]:
    primary_symbol = ""
    superscript = ""
    subscript = ""
    PG = (hadron.parity, hadron.g_parity)
    if hadron.hidden_beauty:
        primary_symbol = BOTTOMONIA[PG]
        if PG != (-1, -1):
            subscript = "b"
    elif hadron.hidden_charm:
        primary_symbol = CHARMONIA[PG]
        if PG != (-1, -1):
            subscript = "c"
    else:
        primary_symbol = get_light_flavourless_meson_symbol(hadron)
        if hadron.isospin == 0 and PG != (-1, -1):
            superscript = "(')"
        elif hadron.isospin == 1:
            superscript = get_charge_symbol(hadron.charge)
    # > The spin J is added as a subscript in the name except for pseudoscalar, vector, and axial vector mesons
    # > or where the quantum numbers are not allowed under the qq̅ picture ("manifestly exotic" values of J^PC)
    if not (
        hadron.has_allowed_qq_qns
        and (hadron.spin, hadron.parity) in {(0, -1), (1, -1), (1, +1)}
    ):
        subscript += str(int(hadron.spin))
    return primary_symbol, superscript, subscript


def name_flavoured_meson(hadron: Hadron) -> tuple[str, str, str]:
    primary_symbol = ""
    superscript = ""
    subscript = ""
    if hadron.open_beauty:
        primary_symbol = "B"
        if hadron.open_charm:
            subscript = "c"
        if hadron.open_strangeness:
            subscript = "s"
    elif hadron.open_charm:
        primary_symbol = "D"
        if hadron.open_strangeness:
            subscript = "s"
    elif hadron.open_strangeness:
        primary_symbol = "K"
    else:
        raise NotImplementedError()
    # > When the spin-parity is in the natural series, J^P = 0^+, 1^−, 2^+ ... , a superscript "*" is added.
    J = int(hadron.spin)
    P = hadron.parity
    if (J % 2 == 0) == (P == +1):
        superscript += "*"
    # Charge superscript always added at the end
    superscript += get_charge_symbol(hadron.charge)
    # > The spin is added as a subscript except for pseudoscalar or vector mesons.
    if (J, P) not in [(0, -1), (1, -1)]:
        subscript += f"{J}"
    return primary_symbol, superscript, subscript


def name_meson(hadron: Hadron) -> tuple[str, str, str]:
    if hadron.is_flavourless:
        return name_flavourless_meson(hadron)
    return name_flavoured_meson(hadron)


def name_baryon(hadron: Hadron) -> tuple[str, str, str]:
    primary_symbol = ""
    superscript = ""
    subscript = ""
    if hadron.is_flavourless:
        if hadron.two_isospin == 1:
            primary_symbol = "N"
        elif hadron.two_isospin == 3:
            primary_symbol = r"\Delta"
        else:
            raise NotImplementedError()
    else:
        S, C, B = hadron.strangeness, hadron.charm, hadron.beauty
        num_heavy = abs(S - C + B)
        if num_heavy == 1:
            primary_symbol = {
                2: r"\Sigma",
                0: r"\Lambda",
            }[hadron.two_isospin]
        elif num_heavy == 2:
            primary_symbol = r"\Xi"
        elif num_heavy == 3:
            primary_symbol = r"\Omega"
        else:
            raise NotImplementedError()
        subscript += "b" * abs(B)
        subscript += "c" * abs(C)
    superscript += get_charge_symbol(hadron.charge)
    return primary_symbol, superscript, subscript


def alternative_name(hadron: Hadron) -> Optional[tuple[str, str, str, bool]]:
    "Cover cases where a specific state has an alternative/traditional name that is an exception to the scheme"
    if all(
        [
            hadron.is_conventional,
            hadron.is_baryon,
            hadron.is_flavourless,
            hadron.two_spin == 1,
            hadron.two_isospin == 1,
            hadron.parity == +1,
        ]
    ):
        symbol = {
            1: "p",
            0: "n",
        }[abs(hadron.charge)]
        bar = hadron.baryon_number < 0
        return symbol, "", "", bar
    if all(
        [
            hadron.is_conventional,
            hadron.is_meson,
            hadron.is_flavourless,
            hadron.hidden_charm,
            hadron.two_spin == 2,
            hadron.parity == -1,
            hadron.c_parity == -1,
        ]
    ):
        return r"J/\psi", "", "", False
    return None
