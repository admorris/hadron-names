###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import warnings
import xml.etree.ElementTree as ET

import js

from hadron_names import Hadron, name_hadron
from hadron_names.exceptions import (
    GParityError,
    IsospinError,
    ParityError,
    QuarkContentError,
    SpinError,
)
from hadron_names.utils import get_possible_isospins, is_quark, names_to_particles

P_NAMES = {1: "+", -1: "&minus;"}


def get_element(elem_id: str) -> ET.Element:
    e = Element(elem_id)  # noqa: F821
    if e is None:
        raise RuntimeError(f'Could not find element with id="{elem_id}"')
    return e


def print_alert(message: str, append: str = False, variant: str = "primary") -> None:
    alert = ET.Element("div", {"class": f"alert alert-{variant}", "role": "alert"})
    alert.text = message
    content = ET.tostring(alert, encoding="unicode")
    get_element("alerts").write(content, append=append)


def get_int(elem_id: str, multiplier: int = 1) -> int:
    e = get_element(elem_id)
    if e.value == "":
        return 0
    return int(multiplier * float(e.value))


def get_parity(elem_id: str) -> int:
    e = get_element(elem_id)
    return 1 if e.element.checked else -1


def get_str(elem_id: str) -> str:
    e = get_element(elem_id)
    return e.value


def get_quarks():
    quark_string = get_str("quarks")
    quark_names = [f"anti-{q.lower()}" if q.isupper() else q for q in quark_string]
    return names_to_particles(quark_names)


def clear_element(elem_id: str) -> None:
    e = get_element(elem_id)
    e.clear()


def uncheck_element(elem_id: str) -> None:
    e = get_element(elem_id)
    if e.element.checked:
        e.element.checked = False


def clear_output() -> None:
    clear_element("alerts")
    clear_element("target")
    clear_element("target-src")
    clear_element("target-link")


def clear_all() -> None:
    for elem_id in ["quarks", "spin", "isospin"]:
        clear_element(elem_id)
    for elem_id in ["parity", "g-parity"]:
        uncheck_element(elem_id)
    clear_output()
    format_qn_field()
    format_quarks_field()
    clear_validation()


def format_quarks_field():
    raw_quarks = get_str("quarks")
    allowed_quarks = "".join([q for q in raw_quarks if is_quark(q.lower())][:5])
    pretty_quarks = re.sub(r"([A-Z]{1})", r"\1&#x305;", allowed_quarks).lower()
    get_element("quarks").element.value = allowed_quarks
    get_element("quark-content").write(pretty_quarks, append=False)
    format_isospin(allowed_quarks)


def format_isospin(quark_str):
    select = get_element("isospin")
    options = []
    for two_I in get_possible_isospins(get_quarks()):
        I_str = str(two_I / 2.0).removesuffix(".0")
        option = ET.Element("option", {"value": I_str})
        option.text = I_str
        options += [ET.tostring(option, encoding="unicode")]
    select.element.disabled = len(options) < 2
    select.element.innerHTML = "\n".join(options)


def spin_str(two_spin: int) -> str:
    return (
        f"{two_spin/2:.0f}"
        if two_spin % 2 == 0
        else f"<sup>{two_spin:.0f}</sup>&frasl;<sub>2</sub>"
    )


def parity_str(parities: list[int]) -> str:
    return "".join([P_NAMES[p] for p in filter(lambda x: x in P_NAMES, parities)])


def format_qn_field():
    for elem_id in ["parity", "g-parity"]:
        get_element(f"{elem_id}-button").write(P_NAMES[get_parity(elem_id)])
    two_spin = get_int("spin", 2)
    two_isospin = get_int("isospin", 2)
    parity = get_parity("parity")
    g_parity = get_parity("g-parity")
    c_parity = g_parity * (-1) ** (two_isospin / 2)
    J_str = spin_str(two_spin)
    PC_str = parity_str([parity, c_parity])
    I_str = spin_str(two_isospin)
    G_str = parity_str([g_parity])
    message = f"{I_str}<sup>{G_str}</sup>{J_str}<sup>{PC_str}</sup>"
    get_element("quantum-numbers").write(message, append=False)


def handle_quarks_change():
    format_quarks_field()
    n_quarks = len(get_str("quarks"))
    e = get_element("spin").element
    e.value = get_int("spin") + 0.5 * (n_quarks % 2)
    e.min = 0.5 * (n_quarks % 2)
    format_qn_field()
    validate()


def handle_spin_change():
    format_qn_field()
    validate()


def handle_isospin_change():
    format_qn_field()
    validate()


def handle_parity_change():
    format_qn_field()
    validate()


def handle_c_parity_change():
    format_qn_field()
    validate()


def clear_validation():
    for elem_id in [
        "spin",
        "isospin",
        "parity",
        "g-parity",
        "quarks",
    ]:
        e = get_element(elem_id).element
        while e.className.endswith(" is-invalid"):
            e.className = e.className.removesuffix(" is-invalid")
        e.setCustomValidity("")


def set_invalid(elem_id, message):
    e = get_element(elem_id).element
    if not e.className.endswith(" is-invalid"):
        e.className += " is-invalid"
    e.setCustomValidity(message)


def validate():
    clear_validation()
    print_name()


def print_link(hadron):
    try:
        if hadron.is_meson:
            pdg_node = "MXXX"
            if hadron.is_flavourless:
                if hadron.hidden_beauty:
                    pdg_node += "030"
                elif hadron.hidden_charm:
                    pdg_node += "025"
                else:
                    pdg_node += "005"
            else:
                if hadron.open_beauty:
                    if hadron.open_charm:
                        pdg_node += "049"
                    elif hadron.open_strangeness:
                        pdg_node += "046"
                    else:
                        pdg_node += "045"
                elif hadron.open_charm:
                    if hadron.open_strangeness:
                        pdg_node += "040"
                    else:
                        pdg_node += "035"
                elif hadron.open_strangeness:
                    pdg_node += "020"
        elif hadron.is_baryon:
            pdg_node = "BXXX"
            if hadron.is_exotic:
                pdg_node += "012"
            elif hadron.open_beauty:
                pdg_node += {(1, 0): "045"}[abs(hadron.beauty), abs(hadron.charm)]
            elif hadron.open_charm:
                pdg_node += {1: "040", 2: "043"}[abs(hadron.charm)]
            else:
                pdg_node += {
                    3: "035",
                    2: "030",
                    1: {0: "020", 1: "025"}[int(hadron.isospin)],
                    0: {0: "005", 1: "010"}[int(hadron.isospin - 0.5)],
                }[abs(hadron.strangeness)]
        else:
            print_alert("Hadron is neither a meson nor a baryon", variant="danger")
        link = ET.Element(
            "a",
            {
                "target": "_blank",
                "class": "link-primary",
                "href": "https://pdglive.lbl.gov/"
                + f"ParticleGroup.action?init=0&node={pdg_node}",
            },
        )
        link.text = "View in pdgLive"
        content = ET.tostring(link, encoding="unicode")
        get_element("target-link").write(content, append=True)
    except KeyError:
        clear_element("target-link")


def print_name():
    two_spin = get_int("spin", 2)
    two_isospin = get_int("isospin", 2)
    parity = get_parity("parity")
    g_parity = get_parity("g-parity")
    quarks = get_quarks()
    clear_output()
    try:
        with warnings.catch_warnings(record=True) as w:
            hadron = Hadron(quarks, two_spin, parity, g_parity, two_isospin)
            latex_name = name_hadron(hadron)
            for warning in w:
                print_alert(str(warning.message), append=True, variant="warning")
        if isinstance(latex_name, list):
            html_name = " or ".join([f"\\({latex}\\)" for latex in latex_name])
            latex_name = " or ".join(latex_name)
        else:
            html_name = f"\\({latex_name}\\)"
        get_element("target-src").write(latex_name, append=True)
        get_element("target").write(html_name, append=True)
        js.MathJax.typeset()
        print_link(hadron)
    except ParityError as e:
        set_invalid("parity", str(e))
    except GParityError as e:
        set_invalid("g-parity", str(e))
    except SpinError as e:
        set_invalid("spin", str(e))
    except IsospinError as e:
        set_invalid("isospin", str(e))
    except QuarkContentError as e:
        set_invalid("quarks", str(e))
    except RuntimeError as e:
        print_alert(str(e), append=True, variant="danger")
    except AssertionError as e:
        print_alert(str(e), append=True, variant="danger")
