# Hadron name resolver

[![pipeline status](https://gitlab.cern.ch/admorris/hadron-names/badges/master/pipeline.svg)](https://gitlab.cern.ch/admorris/hadron-names/-/commits/master) [![coverage report](https://gitlab.cern.ch/admorris/hadron-names/badges/master/coverage.svg)](https://gitlab.cern.ch/admorris/hadron-names/-/commits/master) [![Latest Release](https://gitlab.cern.ch/admorris/hadron-names/-/badges/release.svg)](https://gitlab.cern.ch/admorris/hadron-names/-/releases)

Tool to apply the rules of the PDG hadron naming scheme [^1] and the LHCb exotic hadron naming scheme [^2]

[^1]: https://pdg.lbl.gov/2022/reviews/rpp2022-rev-naming-scheme-hadrons.pdf
[^2]: https://cds.cern.ch/record/2808466
